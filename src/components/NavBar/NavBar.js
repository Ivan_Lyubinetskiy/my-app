import React, { Component } from 'react';
import {NavLink} from 'react-router-dom'
import classes from './NavBar.module.css'

const links = [
  {to: '/', label: 'Authorization', exact: true},
  {to: '/dashboard', label: 'Dashboard 4', exact: false},
  {to: '/sample', label: 'Sample 1', exact: false},
  {to: '/default', label: 'Default 3', exact: false},
  {to: '/report', label: 'Report UAC', exact: false},
  {to: '/untitled', label: 'Untitled', exact: false}
   ]
class NavBar extends Component {

  renderLink() {
    return links.map((link, index) => {
      return (
        <li
          key={index}
          className={classes.Link}
        >
          <NavLink
            to={link.to}
            exact={link.exact}
            onClick={this.clickHandler}
            className={classes.newLink}
            activeClassName={classes.active}
          >
            {link.label}
          </NavLink>
        </li>

      )
    })
  }
  render() {
    return (
      <nav className={classes.NavBar}>
        <div className={classes.cabinet}>
          <h1 className={classes.cabinetHeader}>Cabinet</h1>
        </div>
        <ul className={classes.wrapperLink}>
          { this.renderLink() }
        </ul>
      </nav>
    );
  }

}

export default NavBar
