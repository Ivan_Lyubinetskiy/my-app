import React from 'react';
import classes from './Item.module.css'

const Item = ({data}) => {
  return (
    <tr className={classes.Item}>
      <td className={classes.Item_td}>{data.Channel}</td>
      <td className={classes.Item_td}>{data.Date.slice(0,10)}</td>
      <td className={classes.Item_td}>{data["Media Source"]}</td>
      <td className={classes.Item_td}>{data.ADS_FIVE_WATCHED}</td>
      <td className={classes.Item_td}>{data.ADS_VIDEOAD_WATCHED}</td>
      <td className={classes.Item_td}>{data.Add_Friend_Request}</td>
    </tr>
  )
}

export default Item;
