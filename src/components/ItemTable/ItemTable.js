import React from 'react';
import Item from './Item'
import classes from './ItemTable.module.css'



const ItemTable = ({datas}) => {
  const tableItem = datas.map((data, index) => {
    return (<Item data={data} key={index} />);
  });
  const allRouteData = ['Channel', 'Date', 'Media_Source', 'ADS_FIVE_WATCHED', 'ADS_VIDEOAD_WATCHED', 'Add_Friend_Request']
  const dataItem = allRouteData.map((item, index) =>
  <th
    key={index}
    className={classes.item_th}
  >
    ADS_VIDEOAD_WATCHED
  </th>
)
  return (
    <div className={classes.wrapperTable}>
      <table className={classes.Table}>
        <tbody>
          <tr className={classes.item_tr}>
            { dataItem }
          </tr>

          {tableItem}
        </tbody>
      </table>
    </div>
  );
};


export default ItemTable;
