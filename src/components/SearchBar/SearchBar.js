import React from 'react';
import classes from './SearchBar.module.css'


const SearchBar = ({ term, datas, update}) => {
  const search = e => {
  const value = e.target.value.toLowerCase();

  const filter = datas.filter(data => {
    return data["Media Source"].toLowerCase().includes(value);
  });

  update({
    datas: filter,
    term: value
  });

};

  return (
    <div className={classes.wrapperInput}>
      <p className={classes.upper_text}>Filter Field: Media Source by value</p>
      <input
        className={classes.Input}
        value={term}
        type="text"
        placeholder="Search people by name..."
        onChange={search}
      />
    </div>
  );
}

export default SearchBar
