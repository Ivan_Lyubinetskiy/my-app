import React from 'react';
import Highchartss from 'highcharts/highstock'
import HighchartsReact from 'highcharts-react-official'


const Highchart = ({data, start}) => {

  let year = new Date(start).getFullYear()
  let month = new Date(start).getUTCMonth()
  let day = new Date(start).getDate()
  let newDate = Date.UTC(year, month, day)

const ADS_FIVE_WATCHED = data.map((dt)=>
  dt.ADS_FIVE_WATCHED
)
const ADS_VIDEOAD_WATCHED = data.map((dt)=>
dt.ADS_VIDEOAD_WATCHED
)
const Add_Friend_Request = data.map((dt)=>
dt.Add_Friend_Request
)
const Click_Gifts = data.map((dt)=>
dt.Click_Gifts
)
const Click_Messages = data.map((dt)=>
dt.Click_Messages
)


const options = {
  chart: {
    type: 'spline',
    scrollablePlotArea: {
      minWidth: 600,
      scrollPositionX: 1
    }
  },
  series: [
    {
      name: 'ADS_FIVE_WATCHED',
      data: ADS_FIVE_WATCHED,
      type: 'spline'
    }, {
      name: 'ADS_VIDEOAD_WATCHED',
      data: ADS_VIDEOAD_WATCHED,
      type: 'spline'
    }, {
      name: 'Add_Friend_Request',
      data: Add_Friend_Request,
      type: 'spline'
    }, {
      name: 'Click_Gifts',
      data: Click_Gifts,
      type: 'spline'
    }, {
      name: 'Click_Messages',
      data: Click_Messages,
      type: 'spline'
    }
  ],
  xAxis: {
    type: 'datetime',
    labels: {
      overflow: 'justify'
    }
  },
  yAxis: {
    visible: false
  },
  plotOptions: {
    spline: {
      lineWidth: 3,
      states: {
        hover: {
          lineWidth: 5
        }
      },
      marker: {
        enabled: false
      },
      pointInterval: 3600000,
      pointStart: newDate
    }
  },
}


return (
  <div>
    <HighchartsReact
      highcharts={Highchartss}
      options={options}
    />
  </div>
)}


export default Highchart
