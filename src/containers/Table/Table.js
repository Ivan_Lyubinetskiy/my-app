import React, { Component } from 'react';
import ItemTable from '../../components/ItemTable/ItemTable'
import classes from './Table.module.css'
import allData from '../../data.json'
import SearchBar from '../../components/SearchBar/SearchBar'
import Highcharts from '../../components/Highcharts/Highcharts'
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker'
import {FormControl} from 'react-bootstrap'
import moment from "moment"



class Table extends Component {
  constructor(props){
    super(props);
    let now = new Date();
    let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0,0,0,0));
    let end = moment(start).add(1, "days").subtract(1, "seconds");
    this.state = {
      datas: null,
      active: 0,
      term: '',
      start : start,
      end : end
    }
    this.applyCallback = this.applyCallback.bind(this);
  }
  applyCallback(startDate, endDate){
    let filterByDate = allData.filter((data) => {
      let alldates = new Date(data.Date)
      if(alldates > this.state.start && alldates < this.state.end){
        return data
      }
    })
    this.setState({
      start: startDate,
      end : endDate,
      datas: filterByDate
    }
  )
}
logoutHandler = () => {
  localStorage.clear()
   this.props.history.push('/')
}
componentWillMount() {
  this.initialData = allData
  this.setState({
    datas: this.initialData
  });
}
updateData(config) {
  this.setState(config);
}


render() {
  let now = new Date();
  let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0,0,0,0));
  let end = moment(start).add(1, "days").subtract(1, "seconds");
  let ranges = {
    "Today Only": [moment(start), moment(end)],
    "Yesterday Only": [moment(start).subtract(1, "days"), moment(end).subtract(1, "days")],
    "3 Days": [moment(start).subtract(3, "days"), moment(end)]
  }
  let local = {
    "format":"DD-MM-YYYY HH:mm",
    "sundayFirst" : false
  }
  let maxDate = moment(start).add(24, "hour")
  return (
    <div className={classes.Wrapper}>
      <button
        onClick={this.logoutHandler}
        className={classes.Logout}
      >
      LOGOUT
      </button>
      <Highcharts
        start={this.state.start}
        data={this.state.datas}
       />
      <ItemTable datas={this.state.datas} />
      <div className={classes.bottomWrapper}>
        <SearchBar
            term={this.state.term}
            datas={this.initialData}
            update={this.updateData.bind(this)}
         />
         <DateTimeRangeContainer
             ranges={ranges}
             start={this.state.start}
             end={this.state.end}
             local={local}
             maxDate={maxDate}
             applyCallback={this.applyCallback}
         >
             <FormControl
             id="formControlsTextB"
             type="text"
             label="Text"
             placeholder="Chose date"
             />
         </DateTimeRangeContainer>
       </div>
    </div>
  );
}

}

export default Table;
