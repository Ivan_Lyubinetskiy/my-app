import React, { Component } from 'react';
import classes from './Auth.module.css'

class Auth extends Component {
  constructor(props){
    super(props);
    this.state = {
      email: "test",
      password: "test",
      isValid: true
    }
  }
  handlerEmail = (event) =>{
    if (event.target.value === this.state.email) {
      this.setState({
        isValid: false
      });
    }
  }
  handlerPassword = (event) =>{
    if (event.target.value === this.state.email) {
      this.setState({
        isValid: false
      });
    }
  }
  SubmitHandler = event =>{
    event.preventDefault()
  }
  onSubHandler = () =>{
     this.props.history.push('/dashboard')
     let newAuth = {
       email: "test",
       password: "test"
     }
     localStorage.setItem('Auth', newAuth)
  }


  render() {
    return (
      <div className={classes.Wrapper}>
        <form
        className={classes.Form}
        onSubmit={this.SubmitHandler}
        >
          <h1 className={classes.Header} >Log-in to your account Label</h1>
          <label
            className={classes.Label}
            htmlFor="login"
          >Login Field
          </label>
          <input
            className={classes.Input}
            type="text"
            id="login"
            placeholder="Login"
            onChange={this.handlerEmail}
          />
          <label
            className={classes.Label}
            htmlFor="login"
          >Password Field
          </label>
          <input
            className={classes.Input}
            type="text"
            id="password"
            placeholder="Password"
            onChange={this.handlerPassword}
           />
          <button
            className={classes.Sub}
            type="submit"
            onClick={this.onSubHandler}
            disabled={this.state.isValid}
          >
          LOGIN
          </button>
        </form>
      </div>
    );
  }

}

export default Auth;
