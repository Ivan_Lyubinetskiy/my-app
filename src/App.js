import React, {Component} from 'react'
import Layout from './hoc/Layout/Layout'
import Table from './containers/Table/Table'
import {Route, Switch} from 'react-router-dom'
import Auth from './containers/Auth/Auth'


let auth = localStorage.getItem('Auth')

class App extends Component {
  state = {
    addRoute: false
  }
  componentWillMount() {
    if(auth != null){
      this.setState({
      addRoute: true
      });
    }
    console.log(this.state.addRoute);
  }

  render() {
    let routes = (
      <Switch>
        <Route path="/" exact component={Auth} />
      </Switch>
    )
    if (this.state.addRoute) {
      routes = (
        <Switch>
          <Route path="/dashboard" component={Table} />
          <Route path="/" exact component={Auth} />
        </Switch>
      )
    }
    return (
        <Layout>
            { routes }
        </Layout>
    )
  }
}

export default App
