import React, {Component} from 'react'
import classes from './Layout.module.css'
import NavBar from '../../components/NavBar/NavBar'



class Layout extends Component {
  render() {
    return (
      <div className={classes.Layout}>
      <NavBar />

        <main>
          { this.props.children }
        </main>
      </div>
    )
  }
}

export default Layout
